﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Game.ScriptableObjects;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Cursor = Game.UI.Cursor;

namespace Game {
	[RequireComponent(typeof(Image))]
	public class Hotspot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler {
		[SerializeField] private List<HotspotPossibility> m_PossibleItems;
		[SerializeField] private Item m_CurrentItem;
		[SerializeField] private Image m_HotspotSprite;
		[SerializeField] private float m_HighlightPulseTime;

		private float m_HotspotStartAlpha;
		private Image m_PlacementSprite;
		private Coroutine m_HighlightCoroutine;

		public Animator TheAnimator { get; private set; }

		private void Start() {
			if (m_PossibleItems == null || m_PossibleItems.Count == 0) {
				Debug.LogWarning("The Hotspot " + name + " does not have any possible items.");
			}

			m_PlacementSprite = GetComponent<Image>();

			foreach (HotspotPossibility hp in m_PossibleItems) {
				hp.hotspot = this;
			}

			if (m_HotspotSprite != null) {
				m_HotspotStartAlpha = m_HotspotSprite.color.a;
			}

			TheAnimator = GetComponent<Animator>();
		}
		
		/// <returns>The value of said item in this hotspot. null if it is not possible.</returns>
		public HotspotPossibility GetPossibility(Item item) {
			return m_PossibleItems.FirstOrDefault(hc => hc.item == item);
		}

		/// <summary>
		/// Sets a new item in this Hotspot. Pass null to remove the item. This will not check if the item is possible.
		/// </summary>
		/// <param name="newItem"></param>
		/// <returns>The item that was previously here.</returns>
		public Item SetItem(Item newItem) {
			m_PlacementSprite.sprite = newItem?.m_PlacedSprite;
			if (newItem?.m_PlacedSprite == null) {
				m_PlacementSprite.color = new Color(0, 0, 0, 0);
			} else {
				m_PlacementSprite.color = Color.white;
			}
			Item oldItem = m_CurrentItem;
			m_CurrentItem = newItem;
			return oldItem;
		}

		public Item GetItem() {
			return m_CurrentItem;
		}

		/// <summary>
		/// Call this to highlight this hotspot. Do this when hovering a valid item over this Hotspot.
		/// </summary>
		/// <param name="highlight"></param>
		public void SetHighlight(bool highlight) {
			if (m_HotspotSprite != null) {
				if (highlight) {
					if (m_HighlightCoroutine != null) {
						StopCoroutine(m_HighlightCoroutine);
					}
					m_HighlightCoroutine = StartCoroutine(Highlight());
				} else if (m_HighlightCoroutine != null) {
					m_HotspotSprite.color = new Color(m_HotspotSprite.color.r,
													  m_HotspotSprite.color.g,
													  m_HotspotSprite.color.b,
													  m_HotspotStartAlpha);
					StopCoroutine(m_HighlightCoroutine);
					m_HighlightCoroutine = null;
				}
			}
		}
		
		public void OnPointerEnter(PointerEventData data) {
			if (GetPossibility(Cursor.Instance.GetHeldItem()) != null) {
				SetHighlight(true);
			}
		}
		
		public void OnPointerExit(PointerEventData data) {
			SetHighlight(false);
		}

		public void OnPointerDown(PointerEventData data) {
			Item item = Cursor.Instance.GetHeldItem();
			if (item == null || GetPossibility(item) != null) {
				// Swap held item with hotspot item
				// If m_CurrentItem is null, nothing will happen.
				if (m_CurrentItem != null) {
					GameController.Instance.OnItemRemoved(item, GetPossibility(m_CurrentItem));
					m_CurrentItem.OnRemove(this);
				}
				if (item != null) {
					GameController.Instance.OnItemPlaced(item, GetPossibility(item));
					item.OnPlace(this);
				}
				Cursor.Instance.SetHeldItem(m_CurrentItem);
				SetItem(item);
				SetHighlight(GetPossibility(Cursor.Instance.GetHeldItem()) != null);
			} else {
				// TODO decide what to do when an item is invalid
			}
		}

		private IEnumerator Highlight() {
			float time = 0;

			while (true) { // Coroutine will get stopped with StopCoroutine()
				m_HotspotSprite.color = Color.Lerp(
					new Color(m_HotspotSprite.color.r,
							  m_HotspotSprite.color.g,
							  m_HotspotSprite.color.b,
							  m_HotspotStartAlpha),
					new Color(m_HotspotSprite.color.r,
							  m_HotspotSprite.color.g,
							  m_HotspotSprite.color.b,
							  0), Mathf.PingPong(time, m_HighlightPulseTime));
				time += Time.deltaTime;
				yield return null;
			}
		}
	}

	[Serializable]
	public class HotspotPossibility {
		public Item item;
		public int value;
		[NonSerialized] public Hotspot hotspot;
	}
}
