﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Game {
	public class EndScreenControl : MonoBehaviour {
		[SerializeField] private Image m_GoodEnding, m_BadEnding;

		private Image m_SelectedEnding;

		private void Start() {
			Debug.Log(ScoreData.score);
			if (ScoreData.score < 10) {
				m_SelectedEnding = m_BadEnding;
			} else {
				m_SelectedEnding = m_GoodEnding;
			}

			// Hack because CrossFadeAlpha doesn't work
			// https://stackoverflow.com/a/42333400/3141917
			Action<Image> resetColor = (i) => {
				Color color = i.color;
				color.a = 1;
				i.color = color;
				i.CrossFadeAlpha(0, 0, false);
			};
			resetColor(m_GoodEnding);
			resetColor(m_BadEnding);

			m_SelectedEnding.CrossFadeAlpha(1, 1, false);
		}

		/// <summary>
		/// Static class used to send data from the game scene to the end scene.
		/// </summary>
		public static class ScoreData {
			public static float score;
		}
	}
}
