﻿using System;
using Game.ScriptableObjects;
using Game.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Game {
	public class GameController : Singleton<GameController> {
		[SerializeField] private float m_FadeOutTime;
		[SerializeField] private float m_MoneyAvailable;
		[SerializeField] private Sprite m_HotbarInactiveSprite;
		[SerializeField] private Sprite m_HotbarActiveSprite;

		private float m_Score;

		public float MoneyAvailable {
			get {
				return m_MoneyAvailable;
			}
			private set {
				float oldValue = m_MoneyAvailable;
				m_MoneyAvailable = value;
				OnMoneyChanged.Invoke(this, new ValueChangedEvent<float>(oldValue, value));
			}
		}
		public Sprite HotbarInactiveSprite => m_HotbarInactiveSprite;
		public Sprite HotbarActiveSprite => m_HotbarActiveSprite;

		public float Score {
			get {
				return m_Score;
			}
			set {
				m_Score = value;
				Debug.Log("New score " + value);
			}
		}

		public event EventHandler<ValueChangedEvent<float>> OnMoneyChanged;

		protected override void Awake() {
			base.Awake();

		}

		public void EndButton() {
			FadeGraphic fadescreen = GameObject.FindGameObjectWithTag("FadeScreen")?.GetComponent<FadeGraphic>();
			EndScreenControl.ScoreData.score = Score;
			if (fadescreen != null) {
				fadescreen.StartFading(FadeGraphic.Fade.FadeIn, m_FadeOutTime);
				StartCoroutine(Util.RunAfterSeconds(m_FadeOutTime, () => {
					SceneManager.LoadScene("EndScene");
				}));
			} else {
				SceneManager.LoadScene("EndScene");
			}
		}

		public void OnItemPickup(Item item) {
			MoneyAvailable -= item.Cost;
		}

		public void OnItemPlaced(Item item, HotspotPossibility hp) {
			Score += hp.value;
		}

		public void OnItemRemoved(Item item, HotspotPossibility hp) {
			Score -= hp.value;
		}

		public void OnItemReverted(Item item) {
			MoneyAvailable += item.Cost;
		}
	}
}
