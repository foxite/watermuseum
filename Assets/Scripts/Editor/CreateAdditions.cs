﻿using UnityEditor;
using UnityEngine;

public class CreateAdditions {
	public static readonly string TMPButtonPrefabPath = "Editor/TMPButton";

	[MenuItem("GameObject/UI/TextMeshPro - Button")]
	public static void CreateTMPButton() {
		Object.Instantiate(Resources.Load(TMPButtonPrefabPath), Selection.activeTransform).name = "Button";
	}
}
