﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Game {
	public class MenuButtons : MonoBehaviour {
		[SerializeField] private List<Button> m_Buttons;
		[SerializeField] private Image m_Background;
		[SerializeField] private float m_FadeTime;
		[SerializeField] private string m_LevelSceneName;
		
		public void StartButton() {
			foreach (Button item in m_Buttons) {
				item.gameObject.SetActive(false);
			}
			
			m_Background.CrossFadeColor(Color.black, m_FadeTime, false, false); // Works

			StartCoroutine(Util.RunAfterSeconds(m_FadeTime, () => { SceneManager.LoadScene(m_LevelSceneName); }));
		}

		public void HowToPlayButton() {

		}

		public void QuitButton() {
			Application.Quit();
		}
	}
}
