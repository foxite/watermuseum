﻿using UnityEngine;

namespace Game.UI {
	public class CanvasObjectFollowCursor : MonoBehaviour {
		[SerializeField]
		private bool m_AutomaticOffset;
		[SerializeField]
		private Vector3Int m_Offset;

		private void Start() {
			if (m_AutomaticOffset) {
				m_Offset = Vector3Int.CeilToInt((transform as RectTransform).rect.size / 2);
				m_Offset.Scale(new Vector3Int(1, -1, 1));
			}
		}

		private void Update() {
			transform.position = Input.mousePosition + m_Offset;
		}
	}
}
