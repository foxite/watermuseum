﻿using UnityEngine;
using UnityEngine.UI;

namespace Game.UI {
	public class FadeGraphic : MonoBehaviour {
		[SerializeField] private Fade m_Fade;
		[SerializeField] private float m_Time;

		private Graphic m_TargetGraphic;

		private void Awake() {
			if (m_Fade != Fade.Nothing) {
				m_TargetGraphic = GetComponent<Graphic>();
			}
		}

		private void Update() {
			if (m_Fade == Fade.FadeOut) {
				m_TargetGraphic.CrossFadeAlpha(0, m_Time, false);
			} else if (m_Fade == Fade.FadeIn) {
				m_TargetGraphic.CrossFadeAlpha(1, m_Time, false);
			}
		}

		public void StartFading(Fade fade, float time) {
			m_Fade = fade;
			m_Time = time;
		}

		public enum Fade {
			Nothing, FadeOut, FadeIn
		}
	}
}
