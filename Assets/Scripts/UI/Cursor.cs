﻿using Game.ScriptableObjects;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI {
	public class Cursor : MonoBehaviour {
		public static Cursor Instance { get; private set; }
		
		[SerializeField]
		private Image m_HeldItemImage;
		
		private Item m_CurrentHeldItem;

		public Image HeldItemImage => m_HeldItemImage;
		public Animator HeldItemAnimator { get; private set; }
		
		public Cursor() {
			Instance = this;
		}

		private void Start() {
			HeldItemAnimator = m_HeldItemImage.GetComponent<Animator>();
		}

		public void SetHeldItem(Item item) {
			m_CurrentHeldItem = item;
			if (item == null) {
				m_HeldItemImage.color = new Color(0, 0, 0, 0);
			} else {
				m_HeldItemImage.color = new Color(255, 255, 255, 255);
				m_HeldItemImage.sprite = item.m_HotbarSprite;
			}
		}

		public Item GetHeldItem() {
			return m_CurrentHeldItem;
		}
	}
}
