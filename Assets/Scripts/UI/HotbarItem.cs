﻿using Game.ScriptableObjects;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Game.UI {
	[RequireComponent(typeof(Image))]
	public class HotbarItem : MonoBehaviour, IPointerDownHandler {
		[SerializeField]
		private Item m_Item;

		private bool m_Available = true;

		private void Start() {
			if (m_Item == null) {
				Debug.LogError("HotbarItem " + name + " does not have an Item assigned");
				return;
			}
			
			GetComponent<Image>().sprite = m_Item.m_HotbarSprite;
			Image backgroundImage = transform.parent.GetComponent<Image>();
			
			GameController.Instance.OnMoneyChanged += (o, e) => {
				if (e.newValue >= m_Item.Cost) {
					m_Available = true;
					backgroundImage.sprite = GameController.Instance.HotbarActiveSprite;
				} else {
					m_Available = false;
					backgroundImage.sprite = GameController.Instance.HotbarInactiveSprite;
				}
			};
			
			m_Item.OnStart(this);
		}

		public Item GetItem() {
			return m_Item;
		}

		public void OnPointerDown(PointerEventData data) {
			if (Cursor.Instance.GetHeldItem() == m_Item) {
				Cursor.Instance.SetHeldItem(null);
				m_Item.OnRevert(this);
			} else if (m_Available) {
				Cursor.Instance.SetHeldItem(m_Item);
				m_Item.OnPickup(this);
			}
		}
	}
}
