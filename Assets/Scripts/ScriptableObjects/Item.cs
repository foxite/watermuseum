﻿using Game.UI;
using UnityEngine;
using Cursor = Game.UI.Cursor;

namespace Game.ScriptableObjects {
	[CreateAssetMenu(fileName = "New Item", menuName = "Custom/Item")]
	public class Item : ScriptableObject {
		public string Name;
		public int Cost;
		public Sprite m_PlacedSprite;
		public Sprite m_HotbarSprite;
		public RuntimeAnimatorController m_PlacedSpriteAnimation;
		public RuntimeAnimatorController m_HotbarSpriteAnimation;

		public void OnStart(HotbarItem hotbar) {
			hotbar.GetComponent<Animator>().runtimeAnimatorController = m_HotbarSpriteAnimation;
		}

		public void OnPickup(HotbarItem hotbar) {
			GameController.Instance.OnItemPickup(this);
			Cursor.Instance.HeldItemAnimator.runtimeAnimatorController = m_HotbarSpriteAnimation;
		}

		public void OnRevert(HotbarItem hotbar) {
			GameController.Instance.OnItemReverted(this);
			Cursor.Instance.HeldItemAnimator.runtimeAnimatorController = null;
		}

		public void OnPlace(Hotspot hotspot) {
			hotspot.TheAnimator.runtimeAnimatorController = m_PlacedSpriteAnimation;
		}

		internal void OnRemove(Hotspot hotspot) {
			hotspot.TheAnimator.runtimeAnimatorController = null;
		}
	}
}
